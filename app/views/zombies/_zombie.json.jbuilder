json.extract! zombie, :id, :name, :string, :bio, :text, :age, :created_at, :updated_at
json.url zombie_url(zombie, format: :json)
